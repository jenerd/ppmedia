<?
$slugs = file("slugs", FILE_IGNORE_NEW_LINES);
$domain = "https://invidious.zee.li/embed/";

function embedLink($slug) {
    global $domain;

    return $domain.$slug;
}

function slugIndex($slug) {
    global $slugs;

    for ($i = 0; $i < count($slugs); ++$i)
    {
        if (substr($slugs[$i], 0, strlen($slug) ) === $slug)
            return $i;
    }

    return -1;
}

function nextVideo($next = true) {
    global $slugs;

    if (array_key_exists("v", $_POST) && strlen($_POST["v"]))
    {
        $index = slugIndex($_POST["v"]);

        if ($next)
            ++$index;
        else
            --$index;
        
        if ($index === count($slugs))
            $index = 0;
        elseif ($index < 0)
            $index = count($slugs) - 1;

        return $slugs[$index];
    }

    return $slugs[0];
}

if ($_SERVER["REQUEST_METHOD"] == "POST")
    echo nextVideo(!array_key_exists("p", $_POST));
?>
