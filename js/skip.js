var xhr = null;

getXmlHttpRequestObject = function()
{
    if(!xhr)
        xhr = new XMLHttpRequest();

    return xhr;
};

pick = function(next)
{
    var url = "videolink.php";
    var data = "v=";
    data = data.concat(slug);

    if (next == false)
        data = data.concat("&p");

    xhr = getXmlHttpRequestObject();
    xhr.onreadystatechange = replaceVideo;

    xhr.open("POST", url, true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.send(data);
};

function replaceVideo()
{
    if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200)
    {
        slug = xhr.responseText;
        console.log(domain.concat(slug));
        var e = document.getElementById("embed");
        e.src = domain.concat(slug);

        updateTitle();
    }
}

function updateTitle()
{
    var url = "title.php";
    xhr = getXmlHttpRequestObject();

    xhr.onreadystatechange = function() {
        if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200)
        {
            var data = xhr.responseText.split('𐌸');
            var a = document.getElementById("videoAuthor");
            var t = document.getElementById("videoTitle");
            var l = document.getElementById("channelLink");

            a.innerHTML = data[0];
            t.innerHTML = data[1];
            l.href = "https://invidious.kavin.rocks/channel/" + data[2];
        }
    }

    xhr.open("POST", url, true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.send(slug);
}
