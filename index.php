<!DOCTYPE html><?php
include("videolink.php");
?>
<html style="background-color: indigo;">
<head>
<meta charset="utf-8">
<title>De Piratenpartij in de media</title>
<link rel="shortcut icon" href="img/icon.svg">
<link rel="stylesheet" href="css/stylesheet.css">
<script>
var domain = "<?echo $domain?>";
var slug = "<?echo $slugs[0]?>";
</script>
<script src="js/skip.js"></script>
<!--script src="js/autoplay.js"></script-->
</head>
<body>

    <div id="gallery">
        <div class="arrow left" onclick="pick(false)"></div><?
        echo "<iframe id='embed' src='".embedLink($slugs[0])."' allowfullscreen></iframe>";
        ?><div class="arrow right" onclick="pick(true)"></div>
    </div>
    <h1 id="videoTitle"></h1><br>
    <a id="channelLink" href="" target="_blank"><h2 id="videoAuthor"></h2></a>
    <script>updateTitle();</script>

    <!--div class="caption">
        autoplay<input type="checkbox" id="autoplay" style="display: none;" checked/><label class="check" for="autoplay"></label>
    </div-->

</body>
</html>
